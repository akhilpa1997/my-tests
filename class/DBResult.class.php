<?php
require_once __DIR__ . '/../config/config.php';

class DBResult
{

    private $connection;

    function __construct()
    {

        $this->connection = mysqli_connect(HOST, DB_USERNAME, DB_PASSWORD, DATABASE);
    }

    function __destruct()
    {
        mysqli_close($this->connection);
    }


    /**
     * Create or return a connection to the MySQL server.
     */

    public function getConnection()
    {
        return $this->connection;
    }

    /*  
        Function to check user is valid or not
        @param mixed $params
        @return boolean 
    */
    public function checkUserLogin($params)
    {    
        $userName = $this->cleanInput($params['userName']);
        $userPass = $this->cleanInput($params['userPass']);
        $query = "SELECT * FROM users 
                  WHERE name ='" . $userName . "'  AND password ='" . $userPass . "'"; 
        $result = mysqli_query($this->connection, $query);
        
        if(isset($result->num_rows) && $result->num_rows > 0){
           return mysqli_fetch_array($result,MYSQLI_ASSOC);
        }
        else {
            return false;
        }
    }
     public function checkProductAdd($params)
    {
        
        $name = $this->cleanInput($params['name']);
        $code = $this->cleanInput($params['code']);
        $image = $params['image'];
        $price = $this->cleanInput($params['price']);
        $quantity = $this->cleanInput($params['quantity']);
        $query = "INSERT INTO tblproduct (name,code,image,price,quantity) VALUES('" .$name."','" .$code."','" .$image."','". $price ."','". $quantity ."')" ;
        $result = mysqli_query($this->connection, $query);
        
        if($result){
            return true;
         }
         else {
             return false;
         }

    }
    public function productDetails(){
        print_r($_SESSION); exit;
        if(!isset($_SESSION['PRODUCT_ID'])){
            return array();
        }
      $selectQuery = "SELECT id,name,price,quantity FROM tblproduct
                        WHERE id = '" . $_SESSION['PRODUCT_ID']  . "' ";
        $result = mysqli_query($this->connection, $selectQuery);
        if (isset($result->num_rows) && $result->num_rows > 0) {
            return mysqli_fetch_array($result,MYSQLI_ASSOC);
        } else {
            return array();
        }
    }
    public function checktAddCart($params)
    {
        
        $name = $this->cleanInput($params['name']);
        $price = $this->cleanInput($params['price']);
        $quantity = $this->cleanInput($params['quantity']);
         $query = "INSERT INTO tblproduct (name,price,quantity) VALUES('" .$name."','". $price ."','". $quantity ."')" ;
        $result = mysqli_query($this->connection, $query);
        
        if($result){
            return true;
         }
         else {
             return false;
         }

    }

    function cleanInput($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
}
