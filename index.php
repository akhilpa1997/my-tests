     <?php

        include_once("header.php");
        include_once('class/Login.class.php');

        $objLogin = new Login();
        switch($objLogin->setAction){
        case"";
    ?><div class="container">
            <h1> Login Page </h1>
            <div class="row h-100 justify-content-center align-items-center">

                <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" onsubmit="javascript: return jsValidation(this);">
                    <div class="form-group">
                        <label for="name">name:</label>
                        <input type="text" name="userName" class="form-control" id="email">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Password:</label>
                        <input type="password" name="userPass" class="form-control" id="pwd">
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox"> Remember me</label>
                    </div>
                    <input type="hidden" name="setAction" value="login">
                    <button type="submit" class="btn btn-default">Login</button>
                    <a href="<?php echo htmlspecialchars($_SERVER["PHP_SELF"])."?setAction=register"; ?>">click to add..</a>
                    
                </form>
            </div><?php
                if(isset($objLogin->errors) && !empty($objLogin->errors)){
                    ?><div class="alert alert-danger"><?php
                        print($objLogin->errors);
                    ?></div><?php 
                }
            ?>
        </div>
        <script>
            function jsValidation(objForm) {
                if (trimfield(objForm.name.value) == '') {
                    alert("Name Required!");
                    objForm.name.focus()
                    return false;
                }
                
                if (trimfield(objForm.userPass.value) == '') {
                    alert("Valid Password Required!");
                    objForm.password.focus()
                    return false;
                }
                return true;

            function trimfield(str) {
                return str.replace(/^\s+|\s+$/g, '');
            }
        </script><?php
        break;
        case"register";

        ?><div class="container">
        <h1> Add product </h1>
        <div class="row h-100 justify-content-center align-items-center">

            <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" onsubmit="javascript: return jsValidation(this);">
                <div class="form-group">
                    <label for="name">Product name :</label>
                    <input type="text" name="name" class="form-control" id="name">
                </div>
                <div class="form-group">
                    <label for="code">Product code :</label>
                    <input type="text" name="code" class="form-control" id="code">
                </div>
                <div class="form-group">
                    <label for="file">Product image :</label>
                        
                    <input type="file" name="image">  
                        
                     
                </div>
                <div class="form-group">
                    <label for="Price"> Product Price:</label>
                    <input type="number" name="price" class="form-control" id="price">
                </div>
                
                <div class="form-group">
                    <label for="qty">Product Quantity:</label>
                    <input type="text" name="quantity" class="form-control" id="qty">
                </div>
                
                <input type="hidden" name="setAction" value="addProduct">
                <button type="submit" class="btn btn-default">Add</button>
                
            </form>
        </div><?php
                if(isset($objRegister->errors)&& !empty($objRegister->errors)){
                    ?><div class = "alert alert-danger"><?php
                    print($objRegister->errors);
                    ?></div><?php
                }

        ?>
        <script>
            function jsValidation(objForm) {
                if (trimfield(objForm.name.value) == '') {
                    alert("Name Required!");
                    objForm.name.focus()
                    return false;
                }
                if (trimfield(objForm.code.value) == '') {
                    alert("code Required!");
                    objForm.name.focus()
                    return false;
                }
                if (trimfield(objForm.image.value) == '') {
                    alert("image Required!");
                    objForm.name.focus()
                    return false;
                }
                if (trimfield(objForm.price.value) == '') {
                    alert("price Required!");
                    objForm.price.focus()
                    return false;
                }
                if (trimfield(objForm.quantity.value) == '') {
                    alert("quantity Required!");
                    objForm.quantity.focus()
                    return false;
                }
                return true;
            }

            
            function trimfield(str) {
                return str.replace(/^\s+|\s+$/g, '');
            }

     </script><?php
        break;
      
        //switch close
        }
        ?>


        <?php
            include_once "footer.php";
         ?>